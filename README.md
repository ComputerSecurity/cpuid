cpuid.py
========

Pure Python library for accessing information about x86 processors
by querying the [CPUID](http://en.wikipedia.org/wiki/CPUID)
instruction. Well, not exactly pure Python...

It works by allocating a small piece of virtual memory, copying
a raw x86 function to that memory, giving the memory execute
permissions and then calling the memory as a function. The injected
function executes the CPUID instruction and copies the result back
to a ctypes.Structure where is can be read by Python.

It should work fine on both 32 and 64 bit versions of Windows and Linux
running x86 processors. Apple OS X and other BSD systems should also work,
not tested though...

In addition, as the microcode version is not exposed via CPUID, it can read
the microcode version from /sys/devices/system/cpu/cpu0/microcode/version

Examples
--------
Getting info with eax=0:

    import cpuid

    q = cpuid.CPUID()
    eax, ebx, ecx, edx = q(0)

Running the files:

    $ python cpuid.py
    06-3a-09 (0x000306a9) 0x21 IBRS/IBPB STIBP L1D_FLUSH SSBD MD_CLEAR

    $ % python cpuid.py -v
    Vendor ID : GenuineIntel
    CPU name  : Intel(R) Core(TM) i5-3230M CPU @ 2.60GHz
    Signature : 06-3a-09 (0x000306a9)
    Microcode : 0x21
    Features  : IBRS/IBPB STIBP L1D_FLUSH SSBD MD_CLEAR

    $ % python cpuid.py --dump
    CPUID    A        B        C        D
    00000000 0000000d 756e6547 6c65746e 49656e69
    00000001 000306a9 01100800 7fbae3bf bfebfbff
    00000002 76035a01 00f0b2ff 00000000 00ca0000
    00000003 00000000 00000000 00000000 00000000
    00000004 1c004121 01c0003f 0000003f 00000000
    00000005 00000040 00000040 00000003 00021120
    00000006 00000077 00000002 00000009 00000000
    00000007 00000000 00000281 00000000 9c000400
    00000008 00000000 00000000 00000000 00000000
    00000009 00000000 00000000 00000000 00000000
    0000000a 07300403 00000000 00000000 00000603
    0000000b 00000001 00000002 00000100 00000001
    0000000c 00000000 00000000 00000000 00000000
    0000000d 00000007 00000340 00000340 00000000
    80000000 80000008 00000000 00000000 00000000
    80000001 00000000 00000000 00000001 28100800
    80000002 20202020 49202020 6c65746e 20295228
    80000003 65726f43 294d5428 2d356920 30333233
    80000004 5043204d 20402055 30362e32 007a4847
    80000005 00000000 00000000 00000000 00000000
    80000006 00000000 00000000 01006040 00000000
    80000007 00000000 00000000 00000000 00000100
    80000008 00003024 00000000 00000000 00000000

# -*- coding: utf-8 -*-
#
# Based on https://github.com/flababah/cpuid.py:
# ---
#     Copyright (c) 2014-2018 Anders Høst
#     Under MIT Licence
# ---
#  Copyright (c) 2019 CERN
#
#  The MIT License (MIT)
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of
#  this software and associated documentation files (the "Software"), to deal in
#  the Software without restriction, including without limitation the rights to
#  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#  the Software, and to permit persons to whom the Software is furnished to do so,
#  subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# --
#  In applying this license, CERN does not waive the privileges and immunities
#  granted to it by virtue of its status as an Intergovernmental Organization or
#  submit itself to any jurisdiction.

from __future__ import print_function

import platform
import os
import mmap
import ctypes
import struct
from ctypes import c_uint32, c_int, c_long, c_ulong, c_size_t, c_void_p, POINTER, CFUNCTYPE

# Posix x86_64:
# Three first call registers : RDI, RSI, RDX
# Volatile registers         : RAX, RCX, RDX, RSI, RDI, R8-11

# Windows x86_64:
# Three first call registers : RCX, RDX, R8
# Volatile registers         : RAX, RCX, RDX, R8-11

# cdecl 32 bit:
# Three first call registers : Stack (%esp)
# Volatile registers         : EAX, ECX, EDX

_POSIX_64_OPC = [
        0x53,                    # push   %rbx
        0x89, 0xf0,              # mov    %esi,%eax
        0x89, 0xd1,              # mov    %edx,%ecx
        0x0f, 0xa2,              # cpuid
        0x89, 0x07,              # mov    %eax,(%rdi)
        0x89, 0x5f, 0x04,        # mov    %ebx,0x4(%rdi)
        0x89, 0x4f, 0x08,        # mov    %ecx,0x8(%rdi)
        0x89, 0x57, 0x0c,        # mov    %edx,0xc(%rdi)
        0x5b,                    # pop    %rbx
        0xc3                     # retq
]

_WINDOWS_64_OPC = [
        0x53,                    # push   %rbx
        0x89, 0xd0,              # mov    %edx,%eax
        0x49, 0x89, 0xc9,        # mov    %rcx,%r9
        0x44, 0x89, 0xc1,        # mov    %r8d,%ecx
        0x0f, 0xa2,              # cpuid
        0x41, 0x89, 0x01,        # mov    %eax,(%r9)
        0x41, 0x89, 0x59, 0x04,  # mov    %ebx,0x4(%r9)
        0x41, 0x89, 0x49, 0x08,  # mov    %ecx,0x8(%r9)
        0x41, 0x89, 0x51, 0x0c,  # mov    %edx,0xc(%r9)
        0x5b,                    # pop    %rbx
        0xc3                     # retq
]

_CDECL_32_OPC = [
        0x53,                    # push   %ebx
        0x57,                    # push   %edi
        0x8b, 0x7c, 0x24, 0x0c,  # mov    0xc(%esp),%edi
        0x8b, 0x44, 0x24, 0x10,  # mov    0x10(%esp),%eax
        0x8b, 0x4c, 0x24, 0x14,  # mov    0x14(%esp),%ecx
        0x0f, 0xa2,              # cpuid
        0x89, 0x07,              # mov    %eax,(%edi)
        0x89, 0x5f, 0x04,        # mov    %ebx,0x4(%edi)
        0x89, 0x4f, 0x08,        # mov    %ecx,0x8(%edi)
        0x89, 0x57, 0x0c,        # mov    %edx,0xc(%edi)
        0x5f,                    # pop    %edi
        0x5b,                    # pop    %ebx
        0xc3                     # ret
]

is_windows = os.name == "nt"
is_64bit   = ctypes.sizeof(ctypes.c_voidp) == 8

class CPUID_struct(ctypes.Structure):
    _fields_ = [(r, c_uint32) for r in ("eax", "ebx", "ecx", "edx")]

class CPUID(object):
    def __init__(self):
        if platform.machine() not in ("AMD64", "x86_64", "x86", "i686"):
            raise SystemError("Only available for x86")

        if is_windows:
            if is_64bit:
                # VirtualAlloc seems to fail under some weird
                # circumstances when ctypes.windll.kernel32 is
                # used under 64 bit Python. CDLL fixes this.
                self.win = ctypes.CDLL("kernel32.dll")
                opc = _WINDOWS_64_OPC
            else:
                # Here ctypes.windll.kernel32 is needed to get the
                # right DLL. Otherwise it will fail when running
                # 32 bit Python on 64 bit Windows.
                self.win = ctypes.windll.kernel32
                opc = _CDECL_32_OPC
        else:
            opc = _POSIX_64_OPC if is_64bit else _CDECL_32_OPC

        size = len(opc)
        code = (ctypes.c_ubyte * size)(*opc)

        if is_windows:
            self.win.VirtualAlloc.restype = c_void_p
            self.win.VirtualAlloc.argtypes = [ctypes.c_void_p, ctypes.c_size_t, ctypes.c_ulong, ctypes.c_ulong]
            self.addr = self.win.VirtualAlloc(None, size, 0x1000, 0x40)
            if not self.addr:
                raise MemoryError("Could not allocate RWX memory")
        else:
            self.libc = ctypes.cdll.LoadLibrary(None)
            self.libc.mmap.restype = ctypes.c_void_p
            self.libc.mmap.argtypes = [c_void_p, c_size_t, c_int, c_int, c_int, c_int]
            self.addr = self.libc.mmap(0, size, mmap.PROT_READ | mmap.PROT_WRITE | mmap.PROT_EXEC, mmap.MAP_PRIVATE | mmap.MAP_ANONYMOUS, -1, 0)
            if self.addr == 0xffffffffffffffff:
                raise MemoryError("Could not mmap memory")
        ctypes.memmove(self.addr, code, size)

        func_type = CFUNCTYPE(None, POINTER(CPUID_struct), c_uint32, c_uint32)
        self.func_ptr = func_type(self.addr)

    def __call__(self, eax, ecx=0):
        struct = CPUID_struct()
        self.func_ptr(struct, eax, ecx)
        return [struct.eax, struct.ebx, struct.ecx, struct.edx]

    def __del__(self):
        if is_windows:
            self.win.VirtualFree.restype = c_long
            self.win.VirtualFree.argtypes = [c_void_p, c_size_t, c_ulong]
            self.win.VirtualFree(self.addr, 0, 0x8000)


def cpu_vendor(cpu):
    _, b, c, d = cpu(0)
    return struct.pack("III", b, d, c).decode("utf-8")

def cpu_name(cpu):
    return "".join((struct.pack("IIII", *cpu(0x80000000 + i)).decode("utf-8")
            for i in range(2, 5))).strip()

def cpu_signature(cpu):
    raw = cpu(1)[0]
    stepping = raw & 0xf
    model = (raw & 0xf0) >> 4
    family = (raw & 0xf00) >> 8
    if family in [6, 15]:
        model += (raw & 0xf0000) >> 12
        if family == 15:
            family += (raw & 0xff00000) >> 20
    return (raw, family, model, stepping)

"""
@param {leaf} %eax
@param {sublead} %ecx, 0 in most cases
@param {reg_idx} idx of [%eax, %ebx, %ecx, %edx], 0-based
@param {bit} bit of reg selected by {reg_idx}, 0-based
"""
def is_set(cpu, leaf, subleaf, reg_idx, bit):
    regs = cpu(leaf, subleaf)
    return bool((1 << bit) & regs[reg_idx])

def feature(cpu, name, leaf, subleaf, reg_idx, bit):
    if is_set(cpu, leaf, subleaf, reg_idx, bit):
        return name
    else:
        return " " * len(name)

def valid_inputs(cpuid):
    for eax in (0x0, 0x80000000):
        highest, _, _, _ = cpuid(eax)
        while eax <= highest:
            regs = cpuid(eax)
            yield (eax, regs)
            eax += 1

if __name__ == "__main__":
    import sys
    cpu = CPUID()
    if len(sys.argv) >= 2 and sys.argv[1] == "--dump":
        print(" ".join(x.ljust(8) for x in ("CPUID", "A", "B", "C", "D")).strip())
        for eax, regs in valid_inputs(cpu):
            print(" ".join(format(x, "0>8x") for x in [eax] + regs))
        sys.exit(0)
    security_features = [
        feature(cpu, "IBRS/IBPB", 7, 0, 3, 26),
        feature(cpu, "STIBP", 7, 0, 3, 27),
        feature(cpu, "L1D_FLUSH", 7, 0, 3, 28),
        feature(cpu, "SSBD", 7, 0, 3, 31),
        feature(cpu, "MD_CLEAR", 7, 0, 3, 10),
    ]
    signature = "{1:0>2x}-{2:0>2x}-{3:0>2x} (0x{0:0>8x})".format(*cpu_signature(cpu))
    try:
        with open('/sys/devices/system/cpu/cpu0/microcode/version') as f_in:
            microcode_version = f_in.read().strip()
    except:
        microcode_version = "unknown"
    if len(sys.argv) >= 2 and sys.argv[1] == "-v":
        print("Vendor ID : {0}".format(cpu_vendor(cpu)))
        print("CPU name  : {0}".format(cpu_name(cpu)))
        print("Signature : {0}".format(signature))
        print("Microcode : {0}".format(microcode_version))
        print("Features  : {0}".format(" ".join(security_features)))
    else:
        print(" ".join([signature, microcode_version] + security_features))
